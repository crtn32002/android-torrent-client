package com.github.axet.torrentclient.app;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import libtorrent.Buffer;

public class MetainfoBuilder implements libtorrent.MetainfoBuilder {
    Uri parent;
    Uri u;
    Storage storage;
    ContentResolver resolver;
    ArrayList<Info> list = new ArrayList<>();

    public static class Info {
        public String name;
        public long size;
    }

    public MetainfoBuilder(Uri parent, Storage storage, Uri u) {
        this.parent = parent;
        this.storage = storage;
        this.resolver = storage.getContext().getContentResolver();
        this.u = u;
        walk(u, u);
        String s = u.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE) && list.size() == 1) { // fix one file torrent
            File f = new File(Storage.getFile(u), list.get(0).name);
            this.u = Uri.fromFile(f);
            this.parent = Uri.fromFile(f.getParentFile());
        }
    }

    @Override
    public long filesCount() throws Exception {
        return list.size();
    }

    void walk(Uri root, Uri u) {
        ArrayList<Storage.Node> nn = Storage.walk(storage.getContext(), root, u);
        for (Storage.Node n : nn) {
            if (n.dir) {
                if (!n.uri.equals(u)) // skip initial folder
                    walk(root, n.uri);
            } else {
                Info info = new Info();
                info.name = n.name;
                info.size = n.size;
                list.add(info);
            }
        }
    }

    @Override
    public long filesLength(long i) {
        return list.get((int) i).size;
    }

    @Override
    public String filesName(long i) {
        return list.get((int) i).name;
    }

    @Override
    public String name() {
        return Storage.getName(storage.getContext(), u);
    }

    @Override
    public long readFileAt(String path, Buffer buf, long off) throws Exception {
        String s = u.getScheme();
        if (s.equals(ContentResolver.SCHEME_CONTENT)) {
            Uri u = Storage.child(storage.getContext(), this.u, path);
            ParcelFileDescriptor fd = resolver.openFileDescriptor(u, "rw"); // rw to make it file request (r or w can be a pipes)
            FileInputStream fis = new FileInputStream(fd.getFileDescriptor());
            FileChannel c = fis.getChannel();
            c.position(off);
            ByteBuffer bb = ByteBuffer.allocate((int) buf.length());
            c.read(bb);
            long l = c.position() - off;
            c.close();
            bb.flip();
            buf.write(bb.array(), 0, l);
            return l;
        } else if (s.equals(ContentResolver.SCHEME_FILE)) {
            File f = new File(Storage.getFile(parent), path);
            RandomAccessFile r = new RandomAccessFile(f, "r");
            r.seek(off);
            int l = (int) buf.length();
            long rest = r.length() - off;
            if (rest < l)
                l = (int) rest;
            byte[] b = new byte[l];
            int a = r.read(b);
            if (a != l)
                throw new RuntimeException("unable to read a!=l " + a + "!=" + l);
            r.close();
            long k = buf.write(b, 0, l);
            if (l != k)
                throw new RuntimeException("unable to write l!=k " + l + "!=" + k);
            return l;
        } else {
            throw new Storage.UnknownUri();
        }
    }

    @Override
    public String root() {
        return parent.toString();
    }
}
