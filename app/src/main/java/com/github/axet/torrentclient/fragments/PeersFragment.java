package com.github.axet.torrentclient.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.widgets.EmptyRecyclerView;
import com.github.axet.wget.SpeedInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import libtorrent.Libtorrent;
import libtorrent.Peer;

public class PeersFragment extends Fragment implements MainActivity.TorrentFragmentInterface {
    View v;
    EmptyRecyclerView list;

    HashMap<String, SpeedInfo> dinfo = new HashMap<>();
    HashMap<String, SpeedInfo> uinfo = new HashMap<>();

    Peers peers;

    public static class PeerHolder extends RecyclerView.ViewHolder {
        TextView addr;
        TextView stats;
        TextView name;
        TextView d;
        TextView u;

        public PeerHolder(View view) {
            super(view);
            addr = (TextView) view.findViewById(R.id.torrent_peer_addr);
            stats = (TextView) view.findViewById(R.id.torrent_peer_stats);
            name = (TextView) view.findViewById(R.id.torrent_peer_name);
            d = (TextView) view.findViewById(R.id.torrent_peer_downloaded);
            u = (TextView) view.findViewById(R.id.torrent_peer_uploaded);
        }
    }

    static class SortPeers implements Comparator<Peer> {
        @Override
        public int compare(Peer p1, Peer p2) {
            return p1.getAddr().compareTo(p2.getAddr());
        }
    }

    class Peers extends RecyclerView.Adapter<PeerHolder> {
        ArrayList<Peer> ff = new ArrayList<>();

        @Override
        public int getItemCount() {
            return ff.size();
        }

        public Peer getItem(int i) {
            return ff.get(i);
        }

        @Override
        public PeerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View view = inflater.inflate(R.layout.torrent_peers_item, parent, false);
            return new PeerHolder(view);
        }

        @Override
        public void onBindViewHolder(PeerHolder h, int position) {
            Peer f = getItem(h.getAdapterPosition());

            String a = f.getAddr();

            SpeedInfo di = dinfo.get(a);
            if (di == null) {
                di = new SpeedInfo();
                di.start(f.getDownloaded());
                dinfo.put(a, di);
            } else {
                di.step(f.getDownloaded());
            }

            SpeedInfo ui = uinfo.get(a);
            if (ui == null) {
                ui = new SpeedInfo();
                ui.start(f.getUploaded());
                uinfo.put(a, ui);
            } else {
                ui.step(f.getUploaded());
            }

            long t = getArguments().getLong("torrent");

            String str = "";

            if (Libtorrent.metaTorrent(t))
                str += f.getPiecesCompleted() * 100 / Libtorrent.torrentPiecesCount(t) + "% ";

            if (f.getSupportsEncryption())
                str += "(E)";

            str += "(" + f.getSource().substring(0, 1) + ")";

            h.addr.setText(f.getAddr());
            h.stats.setText(str);
            h.name.setText(f.getName());
            h.d.setText("↓ " + TorrentApplication.formatSize(getContext(), di.getCurrentSpeed()) + getContext().getString(R.string.per_second));
            h.u.setText("↑ " + TorrentApplication.formatSize(getContext(), ui.getCurrentSpeed()) + getContext().getString(R.string.per_second));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.torrent_peers, container, false);

        list = (EmptyRecyclerView) v.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));

        peers = new Peers();

        list.setAdapter(peers);

        DividerItemDecoration div = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        list.addItemDecoration(div);

        View empty = v.findViewById(R.id.empty_list);
        list.setEmptyView(empty);

        update();

        return v;
    }

    @Override
    public void update() {
        long t = getArguments().getLong("torrent");

        long l = Libtorrent.torrentPeersCount(t);

        ArrayList<String> addrs = new ArrayList<>();

        peers.ff.clear();
        for (long i = 0; i < l; i++) {
            Peer p = Libtorrent.torrentPeers(t, i);
            peers.ff.add(p);
            addrs.add(p.getAddr());
        }

        Collections.sort(peers.ff, new SortPeers());

        ArrayList<String> remove = new ArrayList<>();

        for (String k : uinfo.keySet()) {
            if (!addrs.contains(k))
                remove.add(k);
        }

        for (String k : remove) {
            dinfo.remove(k);
            uinfo.remove(k);
        }

        peers.notifyDataSetChanged();
    }

    @Override
    public void close() {
    }
}
