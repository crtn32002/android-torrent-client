package com.github.axet.torrentclient.widgets;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.androidlibrary.preferences.AdminPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.androidlibrary.widgets.UnreadCountDrawable;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.navigators.Torrents;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import libtorrent.Libtorrent;

public class Drawer implements com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener, UnreadCountDrawable.UnreadCount {
    public static final String TAG = Drawer.class.getSimpleName();

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    public static int[] DEVELOPERS = new int[]{0xc38af5bf, 0x3feda1d1}; // 0xc38af5bf release, 0x3feda1d1 debug

    static final long INFO_MANUAL_REFRESH = 5 * AlarmManager.SEC1; // prevent refresh if button hit often then 5 seconds
    static final long INFO_AUTO_REFRESH = 5 * AlarmManager.MIN1; // ping external port on drawer open not often then 5 minutes
    static final long ENGINES_AUTO_REFRESH = 12 * AlarmManager.HOUR1; // auto refresh engines every 12 hours

    Context context;
    MainActivity main;
    Handler handler = new Handler();
    OpenChoicer choicer;

    View navigationHeader;
    com.mikepenz.materialdrawer.Drawer drawer;
    UnreadCountDrawable unread;

    Thread update;
    Thread updateOne;
    int updateOneIndex;

    Thread infoThread;
    List<String> infoOld;
    Boolean infoPort;
    long infoTime; // last time checked

    HashMap<ProxyDrawerItem, ProxyDrawerItem.ViewHolder> viewList = new HashMap<>();

    ItemTouchHelper touchHelper;

    long counter = 1;

    public static boolean signatureCheck(Context context, int[] dd) {
        for (int d : dd) {
            if (signatureCheck(context, d))
                return true;
        }
        return false;
    }

    public static boolean signatureCheck(Context context, int d) {
        try {
            PackageManager pm = context.getPackageManager();
            Signature[] ss = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (Signature s : ss) {
                int hash = s.hashCode();
                if (d == hash)
                    return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public Drawer(final MainActivity main, final Toolbar toolbar) {
        this.context = main;
        this.main = main;

        LayoutInflater inflater = LayoutInflater.from(context);

        navigationHeader = inflater.inflate(R.layout.nav_header_main, null, false);

        drawer = new DrawerBuilder()
                .withActivity(main)
                .withToolbar(toolbar)
                .withHeader(navigationHeader)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withOnDrawerListener(new com.mikepenz.materialdrawer.Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        View view = main.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }
                })
                .withOnDrawerItemClickListener(this)
                .build();

        touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                if (!(viewHolder instanceof SearchDrawerItem.ViewHolder))
                    return false;
                if (!(target instanceof SearchDrawerItem.ViewHolder))
                    return false;

                ItemAdapter<IDrawerItem> ad = drawer.getItemAdapter();
                int i = ad.getFastAdapter().getHolderAdapterPosition(viewHolder);
                int k = ad.getFastAdapter().getHolderAdapterPosition(target);
                ad.move(i, k);

                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (!(viewHolder instanceof SearchDrawerItem.ViewHolder))
                    return makeMovementFlags(0, 0);
                return super.getMovementFlags(recyclerView, viewHolder);
            }
        });
        touchHelper.attachToRecyclerView(drawer.getRecyclerView());

        Drawable navigationIcon = toolbar.getNavigationIcon();
        unread = new UnreadCountDrawable(context, navigationIcon, Drawer.this);
        unread.setPadding(ThemeUtils.dp2px(main, 15));
        toolbar.setNavigationIcon(unread);

        TextView ver = (TextView) navigationHeader.findViewById(R.id.nav_version);
        AboutPreferenceCompat.setVersion(ver);
    }

    public void close() {
    }

    public void setCheckedItem(long id) {
        drawer.setSelection(id, false);
    }

    public boolean isDrawerOpen() {
        return drawer.isDrawerOpen();
    }

    public void openDrawer() {
        drawer.openDrawer();
    }

    public void closeDrawer() {
        drawer.closeDrawer();
    }

    public void updateUnread() {
        unread.update();
    }

    public void updateManager() {
        List<IDrawerItem> list = new ArrayList<>();

        final Torrents torrents = main.getTorrents();
        if (torrents == null) {
            ProgressDrawerItem progress = new ProgressDrawerItem();
            progress.withIdentifier(R.id.progress);
            progress.withSelectable(true);
            progress.withSetSelected(false);
            list.add(progress);
            update(list);
            return;
        }

        PrimaryDrawerItem tt = new PrimaryDrawerItem();
        tt.withIdentifier(R.id.nav_torrents);
        tt.withName(R.string.torrents);
        tt.withIcon(new UnreadCountDrawable(context, R.drawable.ic_storage_black_24dp, torrents));
        tt.withIconTintingEnabled(true);
        tt.withSelectable(true);
        tt.withSetSelected(torrents.navfilter == Torrents.Filter.FILTER_ALL);
        list.add(tt);

        SecondaryDrawerItem ss = new SecondaryDrawerItem();
        ss.withIdentifier(R.id.nav_downloading);
        ss.withName(R.string.status_downloading);
        ss.withIcon(R.drawable.ic_drag_handle_black_24dp);
        ss.withIconTintingEnabled(true);
        ss.withSelectable(true);
        ss.withSetSelected(torrents.navfilter == Torrents.Filter.FILTER_DOWNLOADING);
        list.add(ss);

        ss = new SecondaryDrawerItem();
        ss.withIdentifier(R.id.nav_seeding);
        ss.withName(R.string.status_seeding);
        ss.withIcon(R.drawable.ic_drag_handle_black_24dp);
        ss.withIconTintingEnabled(true);
        ss.withSelectable(true);
        ss.withSetSelected(torrents.navfilter == Torrents.Filter.FILTER_SEEDING);
        list.add(ss);

        ss = new SecondaryDrawerItem();
        ss.withIdentifier(R.id.nav_queued);
        ss.withName(R.string.status_queue);
        ss.withIcon(R.drawable.ic_drag_handle_black_24dp);
        ss.withIconTintingEnabled(true);
        ss.withSelectable(true);
        ss.withSetSelected(torrents.navfilter == Torrents.Filter.FILTER_QUEUED);
        list.add(ss);

        update(list);
    }

    void update(List<IDrawerItem> list) {
        ItemAdapter<IDrawerItem> ad = drawer.getItemAdapter();
        for (int i = ad.getAdapterItemCount() - 1; i >= 0; i--) {
            boolean delete = true;
            for (int k = 0; k < list.size(); k++) {
                if (list.get(k).getIdentifier() == ad.getAdapterItem(i).getIdentifier()) {
                    delete = false;
                    ad.set(ad.getGlobalPosition(i), list.get(k));
                }
            }
            if (delete)
                ad.remove(ad.getGlobalPosition(i));
        }
        for (int k = 0; k < list.size(); k++) {
            boolean add = true;
            for (int i = 0; i < ad.getAdapterItemCount(); i++) {
                if (list.get(k).getIdentifier() == ad.getAdapterItem(i).getIdentifier())
                    add = false;
            }
            if (add)
                ad.add(ad.getGlobalPosition(k), list.get(k));
        }
    }

    public void updateHeader() {
        ArrayList<String> info = new ArrayList<>();
        long c = Libtorrent.portCount();
        for (long i = 0; i < c; i++)
            info.add(Libtorrent.port(i));

        StringBuilder str = new StringBuilder();
        for (String ip : info) {
            str.append(ip);
            str.append("\n");
        }
        final String ss = str.toString().trim();

        TextView textView = (TextView) navigationHeader.findViewById(R.id.torrent_ip);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, ss, Toast.LENGTH_SHORT).show();
            }
        });
        textView.setText(ss);

        View portButton = navigationHeader.findViewById(R.id.torrent_port_button);
        ImageView portIcon = (ImageView) navigationHeader.findViewById(R.id.torrent_port_icon);
        TextView port = (TextView) navigationHeader.findViewById(R.id.torrent_port_text);

        portButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long time = System.currentTimeMillis();
                if (infoTime + INFO_MANUAL_REFRESH < time) {
                    infoTime = time;
                    infoOld = null;
                }
            }
        });

        long time = System.currentTimeMillis();
        if (infoTime + INFO_AUTO_REFRESH < time) {
            infoTime = time;
            infoOld = null;
        }

        if (infoOld == null || !Arrays.equals(info.toArray(), infoOld.toArray())) {
            if (drawer.isDrawerOpen()) { // only probe port when drawer is open
                if (infoThread != null)
                    return;
                infoOld = info;
                infoThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final boolean b = Libtorrent.portCheck();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    infoPort = b;
                                    infoThread = null;
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    infoPort = null;
                                    infoThread = null;
                                }
                            });
                        }
                    }
                }, "Port Check");
                infoThread.start();
                infoPort = false;
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_checking);
            } else {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_closed);
            }
        } else {
            if (infoPort == null) {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_down);
            } else if (infoPort) {
                portIcon.setImageResource(R.drawable.port_ok);
                port.setText(R.string.port_open);
            } else {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_closed);
            }
        }
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        long id = drawerItem.getIdentifier();

        // here only two types of adapters, so setup empty view manually here.

        if (id == R.id.nav_torrents) {
            main.openTorrents(Torrents.Filter.FILTER_ALL);
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_downloading) {
            main.openTorrents(Torrents.Filter.FILTER_DOWNLOADING);
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_seeding) {
            main.openTorrents(Torrents.Filter.FILTER_SEEDING);
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_queued) {
            main.openTorrents(Torrents.Filter.FILTER_QUEUED);
            closeDrawer();
            return true;
        }

        if (drawerItem.getTag() != null) {
            updateManager(); // update proxy list
            closeDrawer();
            return true;
        }

        return true;
    }

    @Override
    public int getUnreadCount() {
        int count = 0;
        Torrents torrents = main.getTorrents();
        if (torrents != null)
            count += torrents.getUnreadCount();
        return count;
    }

    @TargetApi(21)
    public void onActivityResult(int resultCode, Intent data) {
        choicer.onActivityResult(resultCode, data);
    }

    public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        choicer.onRequestPermissionsResult(permissions, grantResults);
    }
}
